.. SMUTHI documentation master file, created by
   sphinx-quickstart on Wed May  3 19:50:09 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: images/logo_cropped.svg
   :width: 40%
   :align: center

Contents
=========

.. toctree::
   :maxdepth: 1

   about_smuthi

.. toctree::
   :maxdepth: 2

   getting_started

.. toctree::
   :maxdepth: 2

   simulation_guidelines

.. toctree::
   :maxdepth: 2

   examples

.. toctree::
   :maxdepth: 1

   smuthi_api

.. toctree::
   :maxdepth: 1

   literature

