The smuthi.linearsystem.tmatrix.nfmds package
=============================================

nfmds
-----

.. automodule:: smuthi.linearsystem.tmatrix.nfmds
    :members:
    :undoc-members:

nfmds.t_matrix_axsym
--------------------

.. automodule:: smuthi.linearsystem.tmatrix.nfmds.t_matrix_axsym
    :members:
    :undoc-members:		